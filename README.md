# typora-theme-仿公文主题

#### 介绍
根据官方主题“GitHub”魔改的仿公文主题，用思源宋体替代了小标宋,因为我好像调用小标宋等字体完全不会起作用，哪位大神如果知道原因请赐教。
字体下载：["NotoSansSC","NotoSerifSC"](https://fonts.google.com/?subset=chinese-simplified)

#### 截图
typora 显示效果

![typora 显示效果](https://images.gitee.com/uploads/images/2021/0619/143635_6b6896e3_8363075.png "dd.png")

导出 PDF 效果

![导出 PDF 效果](https://images.gitee.com/uploads/images/2021/0619/143715_08a0ba0b_8363075.png "图层 0.png")

#### 安装教程

1.  下载 gwstyle.css
2.  从 typora 首选项“文件”-“偏好设置”-“外观”-“打开主题文件夹”。
3.  把主题复制到文件夹中，重新打开 typora ，在“主题”下拉菜单中选择“gwstyle”即可。
4.  落款和日期如果需要右对齐，可以使用以下两种方式实现：

```
<p style="text-align: right"> *****技术有限公司 </p>
<span style="float:right">2021年6月22日</span>
```


